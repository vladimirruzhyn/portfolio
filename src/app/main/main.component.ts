import { Component, OnInit } from '@angular/core';

import {ObjectItemList} from '../progect-mock';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  projectList: Object[];

  constructor() { }

  ngOnInit() {
    this.projectList = ObjectItemList;
  }

}
