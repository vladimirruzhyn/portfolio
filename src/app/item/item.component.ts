import { Component, OnInit } from '@angular/core';
import { ActivatedRoute  } from '@angular/router';
import {ObjectItemList} from '../progect-mock';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  item: Object;

  constructor(
    private _r: ActivatedRoute
  ) { }

  ngOnInit() {

    this._r.params.subscribe(params => {
      let id = params['id'];
      if (ObjectItemList[id-1]) {
        this.item = ObjectItemList[id-1];
      }
      console.log(this.item);
    });
  }

}
