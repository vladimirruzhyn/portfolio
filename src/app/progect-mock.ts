export const ObjectItemList: Object[] = [
    { 
        id: 1, 
        descritpion: 'Management system for preschool institutions', 
        technologies: 'PHP, Joomla, Javascript, JQuery, Selenium, Java, JSON, HTML, CSS, Bootstrap, MySQL', 
        title: 'Management system for preschool institutions',
        imagePath: './assets/babysdays.jpg',
        imageFirstPath: './assets/mobile-babys1.jpg',
        imageSecondPath: './assets/mobile-babys.jpg',
        fullDescription:'<p>A large complex management system for pre-schools.</p>'+
                        '<p>The System consists of 16 modules and more than 250 unique pages. Today, the product is being used by more than 4500 of pre-school institutions.</p>'+        
                        '<p>The system automates all processes - from marks on the mood of a child before paying taxes.</p>'+
                        '<p>The system consists of a web-based platform and a mobile application (Android, iOS)</p>',
        technologiesList:[
            {
                title: 'Web Server',
                content: 'PHP5, Joomla 3, Javascript, Jquery, Bootstrap'
            },
            {
                title: 'Mobile platform',
                content: 'JAVA, Object-C'
            },
            {
                title: 'Automatic testing',
                content: 'JAVA, Selenium, PHPUnit, Jenkin'
            }
        ],
        worck: {
            type: 'list',
            list:[
                'Development of API for mobile platforms', 
                'Development and implementation of automated testing',
                'Web Platform Development', 
                'Development of the structure of the database'
            ]
        }
    },




    { 
        id: 2, 
        descritpion: 'Organic Food Store', 
        technologies: 'PHP, Magento 2, Javascript, JQuery, RequireJS, KnockoutJS, Selenium, Python, Google API, MySQL', 
        title: 'Little lunch',
        imagePath: './assets/littlelunch.jpg',
        imageFirstPath: './assets/littlelunch1.jpg',
        imageSecondPath: './assets/littlelunch2.jpg',
        fullDescription:'<p>Organic Food Store.</p>'+
                        '<p>The shop belongs to "Little Lunch" ltd and sells the products of the company which consists of organic soups, smoothies, and brooks.</p>'+        
                        '<p>The firm is one of the foremost in the European market.</p>'+
                        '<p>The store uses advanced technologies to automate the sales process and track the buyer\'s actions</p>',
        technologiesList:[
            {
                title: 'Web Server',
                content: 'PHP7, Magento 2, Javascript, JQuery, RequireJS, KnockoutJS, MySQL'
            },
            {
                title: 'Automatic testing',
                content: 'JAVA, Selenium, PHPUnit, Python, JMeter'
            },
            {
                title: 'Additional Technologies',
                content: 'Google analytics, Google maps, PayPal, Braintree'
            }
        ],
        worck: {
            type: 'list',
            list:[
                'Development of modules for Magento 2 (Google analytics, Address validation, Payment, etc.)',
                'Development of functional tests',
                'Development and implementation of load testing',
                'Unit Test Development'
            ]
        }
    },
    { 
        id: 3, 
        descritpion: 'Logistics Management System', 
        technologies: 'Python, WS, Backbone, PgSQL, Redis, Mongo', 
        title: 'Logistic',
        imagePath: './assets/logistics.jpg',
        hovColor: '#f44242'
    },
    { 
        id: 4, 
        descritpion: 'Job search in all soy networks and chats', 
        technologies: 'Go, Mongo, Redis, MySQL, Angular 5', 
        title: 'Remote Work',
        imagePath: './assets/remote work.jpg',
        hovColor: '#f44242'
    },
    { 
        id: 5, 
        descritpion: 'Playground for sale photos', 
        technologies: 'ReactJs, NodeJs, MySQL, Modx', 
        title: 'Picstation',
        imagePath: './assets/picstation.jpg',
        imageFirstPath: './assets/picstation1.jpg',
        imageSecondPath: './assets/picstation2.jpg',
        fullDescription:'<p>Playground for sale photos</p>'+
                        '<p>The platform allows you to sell your products as photographers to professionals and amateurs.</p>'+
                        '<p>The system guarantees that the make will be concluded without fraud - the buyer will receive a photo, and the photographer money.'+
                        '<p>The search system allows the buyer to choose the most appropriate ones from a large number of photos. This greatly increases the search efficiency.</p>',
        technologiesList:[
            {
                title: 'Web Server',
                content: 'NodeJs, MySQL, Javascript, ReactJs'
            }
        ],
        worck: {
            type: 'description',
            list:[
                'The project was fully implemented Tartila dev team'
            ]
        },
        hovColor: '#f44242'
    },
    { 
        id: 6, 
        descritpion: 'The platform for finding work / employees and conducting IT projects', 
        technologies: 'Python, Flask, MySQL, Angular 5', 
        title: 'Bit Jobs',
        imagePath: './assets/bit jobs.jpg',
        imageFirstPath: './assets/bit jobs1.jpg',
        imageSecondPath: './assets/bit jobs2.jpg',
        fullDescription:'<p>The platform for finding work / employees and conducting IT projects</p>'+
                        '<p>Bit Jobs includes all the necessary functionality for IT projects, which has a simple and intuitive interface. This allows you to run the project directly in the system.</p>'+
                        '<p>The system contains detailed information about the employee and employer, as well as their work history in the system that allows you to choose the best employees and employers.</p>'+
                        '<p>The system supports payment of work that allows to reduce fraud on the part of the customer.</p>',
        technologiesList:[
            {
                title: 'Web Server',
                content: 'NodeJs, MySQL, Javascript, ReactJs'
            }
        ],
        worck: {
            type: 'description',
            list:[
                'The alpha version of the system with all the functionality was developed by Tartila dev team'
            ]
        },
        hovColor: '#f44242'
    }
  ];