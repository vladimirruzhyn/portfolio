import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-animated-bar',
  templateUrl: './animated-bar.component.html',
  styleUrls: ['./animated-bar.component.css'],
  animations: [
    trigger('visibilityChanged', [
      state('hide', style({
        opacity: 0
      })),
      state('show',   style({
        opacity: 1
      })),
      transition('hide => show', animate('500ms')),
      transition('show => hide', animate('500ms'))
    ])
  ]
})
export class AnimatedBarComponent implements OnInit {
  @Input() id: string;
  @Input() descritpion: string;
  @Input() technologies: string;
  @Input() title: string;
  @Input() hovColor: string;
  @Input() imagePath: string;
  opacity: string = 'hide';
  opacityStatus: boolean = false
  hoverImg() {
    this.opacityStatus = !this.opacityStatus;
    if (this.opacityStatus) {
        this.opacity = 'show';
    } else {
      this.opacity = 'hide';
    }
  }
  constructor() { }

  ngOnInit() {
    this.hovColor = this.hovColor+'B3';
  }

}
