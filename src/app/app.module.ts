import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgStickyModule } from 'ng-sticky';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
//  import { SmoothScrollToDirective, SmoothScrollDirective } from "ng2SmoothScroll";

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { TeamItemComponent } from './team-item/team-item.component';
import { AnimatedBarComponent } from './animated-bar/animated-bar.component';
import { ItemComponent } from './item/item.component';

export const routes: Routes = [ {
    component: MainComponent,
    path: ''
  },
  {
    component: AboutComponent,
    path: 'about'
  },
  {
    component: ContactComponent,
    path: 'contact'
  },
  {
    component: ItemComponent,
    path: 'item/:id'
  },
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    ContactComponent,
    AboutComponent,
    TeamItemComponent,
    AnimatedBarComponent,
    ItemComponent/*,
    SmoothScrollToDirective,
    SmoothScrollDirective,*/
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    RouterModule.forRoot(routes),
    NgStickyModule,
    TooltipModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }