import { Component, OnInit } from '@angular/core';
import { setTheme } from 'ngx-bootstrap/utils';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() {
    setTheme('bs3'); 
  }

  ngOnInit() {
  }

  goToContact() {
    let element = document.getElementById('contact');
    if (element != null) {
      element.scrollIntoView({ block: 'start', behavior: 'smooth' });
    }
  }

}
