webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/about/about.component.css":
/***/ (function(module, exports) {

module.exports = ".content {\n    max-width: 1100px;\n    margin: 0px auto;\n    padding: 0px 14px;\n}\n.title {\n    font-size: 46px;\n    font-weight: 500;\n    line-height: 66px;\n    text-align: center;\n    margin-top: 24px;\n    margin-bottom: 20px;\n    text-transform: uppercase;\n}\n.slogan {\n    font-size: 18px;\n    font-weight: 400;\n    line-height: 30px;\n    margin-bottom: 80px;\n    text-align: center;\n}\n.tagline .row .cell {\n    font-size: 18px;\n    font-weight: 400;\n    line-height: 30px;\n    letter-spacing: 0.4px;\n    /*height: 88px;*/\n    margin-bottom: 30px;\n}\n@media screen and (max-width:465px) {\n    .tagline .row .cell {\n        height: 95px;\n    }\n}\n@media screen and (min-width:466px) and (max-width:750px) {\n    .tagline .row .cell {\n        height: 48px;\n    }\n}\n@media screen and (min-width:750px) and (max-width:1050px) {\n    .tagline .row .cell {\n        height: 125px;\n    }\n}\n.tagline-title {\n    padding-top: 20px;\n}\n.left {\n    float: left;\n}\n.right {\n    float: right;\n}\n.center {\n    margin: 0px auto;\n}\n.line hr {\n    background-color: #01994d;\n    width: 140px;\n    height: 1px;\n    border: 0px;\n    text-align: center;    \n}\n.team-row .title {\n    font-size: 32px;\n    line-height: 42px;\n    font-weight: 700;\n    margin: 0 0 20px 0;\n    text-align: center;\n}\n.team-row .body {\n    text-align: center;\n    margin-top: 90px;\n}"

/***/ }),

/***/ "./src/app/about/about.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"content\">\n<div class=\"row\">\n    <div class=\"col-lg-12 col-xs-12 title\">\n      Tartila dev team is a team of developers of web systems who strive to be the best\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-lg-12 col-xs-12 slogan\">\n        We develop not just websites, but unique products that not only satisfy our customers' requests, but do more.\n    </div>\n  </div>\n  <div class=\"tagline\">\n      <div class=\"tegline-width-title\">\n        <div class=\"line\">\n          <hr/>\n        </div>\n        <div class=\"row\">\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n            <b>\n              GREAT CODE <br/>              \n              A GREAT TEAM\n            </b>\n          </div>\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n            <b>We are ambitious</b> - we are not afraid and tuned on large and interesting projects, throwing us a call.\n          </div>\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n              <b>We are experienced</b> - our staff has produced and provided technical support to many projects from a different direction.\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n            Our development work in the field of IT least 3 years, never stand still and constantly develop to meet the needs of our clients\n          </div>\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n            <b>We are affordable</b> - our pricing policy makes our team available to all. We never charge for what we do\n          </div>\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n              <b>We are sufficient </b> - our team is all you need to realize your project..\n          </div>\n        </div>\n      </div>\n      <div class=\"tegline-width-title\">\n        <div class=\"line\">\n          <hr/>\n        </div>\n        <div class=\"row\">\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n            <b>\n              WITH US EASY<br/>\n              TO DO BUSINESS\n            </b>\n          </div>\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n            <b>We organized</b> - our workflow is designed for maximum performance and quality necessary for you.\n          </div>\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n              <b>We are responsible</b> - we understand what is at stake. Our reputation means everything to us.\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n              Your business means everything to us. We cherish your ideas, and are also always as honest and open in front of you\n          </div>\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n            <b>We are modern</b> - our solutions always comply with modern trends.\n          </div>\n          <div class=\"cell col-lg-4 col-sm-4  col-xs-12\">\n              <b>We are experts</b> - we have already found solutions to all problems that may arise in the course of work, and if not, we'll find them.\n          </div>\n        </div>\n      </div>\n  </div>\n  <div class=\"team-row\">\n    <div class=\"title\">\n      Our team\n    </div>\n    <div class=\"body\">\n        <app-team-item></app-team-item>\n        <app-team-item></app-team-item>\n        <app-team-item></app-team-item>\n        <app-team-item></app-team-item>\n        <app-team-item></app-team-item>\n    </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/about/about.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutComponent = /** @class */ (function () {
    function AboutComponent() {
    }
    AboutComponent.prototype.ngOnInit = function () {
    };
    AboutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-about',
            template: __webpack_require__("./src/app/about/about.component.html"),
            styles: [__webpack_require__("./src/app/about/about.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "./src/app/animated-bar/animated-bar.component.css":
/***/ (function(module, exports) {

module.exports = ".bar {\n    width: 100%;\n    position:relative;\n    display: inline-block;\n}\n\n.bar img {\n    width: 100%;\n}\n\n.content {\n    position:absolute;\n    width:100%;\n    height:100%; \n    bottom: 0px;\n}\n\n.content > div {\n    background-color: #000000B3;\n    width:100%;\n    height:100%; \n    font-weight: 700;\n    font-style: normal;\n    font-size: 4.84375vw;\n    /*line-height: 1.5em;\n    letter-spacing: 0em;*/\n    /*text-align: center;*/\n    color: white;\n    /*padding-top: 17%;*/\n}\n\n/*@media screen and (max-width: 516px) and (min-aspect-ratio: 1/1) {\n    .content >div > div {\n        font-size: 16px;\n    }\n\n    .content .title {\n        padding-top: 30px;\n    }\n}*/\n\n.content .title span {\n    font-weight: 300;\n    font-size: .8em;\n    color: #fff;\n    line-height: 1em;\n}\n\n.content >div > div {\n    font-size: 18px;\n    width: 100%;\n    padding-left: 30px;\n    padding-right: 30px;\n}\n\n.content .title {\n    text-align: center;\n    padding-top: 10%;\n    height: 30%;\n}\n\n.content .descrition {\n    font-weight: 400;\n    padding-top: 0%;\n    height: 15%;\n}\n\n.content .teh-title {\n    /*padding-top: 15px;*/\n    height: 10%;\n}\n\n.content .teh {\n    /*padding-top: 5px;*/\n    font-weight: 400;\n    height: 25%\n}\n\n.content .action {\n    text-align: center;\n    /*padding-top: 95px;*/\n}\n\n.content .action a {\n    font-size: 18px;\n    font-weight: 400px;\n    text-decoration: none;\n    cursor: pointer;\n    color: white;\n    border: 1px solid white;\n    border-radius: 5px;\n    padding: 10px 20px;\n\n}\n\n.content .action a:hover {\n    background: black;\n}\n\n@media screen and (min-width: 769px) {\n    .bar {\n        width: 33.3333333%;\n    }\n}\n\n@media screen and (min-aspect-ratio: 1/1) and (min-width: 1920px) {\n    .content .title {\n        font-size: 93px;\n    }\n}\n\n@media screen and (max-width: 1490px) and (min-width: 1080px) {\n    .content >div > div {\n        font-size: 16px;\n    }\n\n    /*.content .title {\n        padding-top: 30px;\n    }*/\n}\n\n@media screen and (max-width: 1079px) and (min-width: 950px) {\n    .content >div > div {\n        font-size: 13px;\n    }\n\n    /*.content .title {\n        padding-top: 20px;\n    }*/\n\n    .content .action a {\n        font-size: 12px;\n        padding: 8px 15px;\n    \n    }\n}\n\n@media screen and (max-width: 950px) and (min-width: 750px) {\n    .content >div > div {\n        font-size: 10px;\n    }\n\n    /*.content .title {\n        padding-top: 20px;\n    }*/\n\n    .content .action a {\n        font-size: 10px;\n        padding: 8px 15px;\n    \n    }\n}\n\n@media screen and (max-width: 750px) and (min-width: 630) {\n    .content >div > div {\n        font-size: 20px;\n    }\n\n    /*.content .title {\n        padding-top: 80px;\n    }\n\n    .content .action {\n        padding-top: 85px;\n    }*/\n\n    .content .action a {\n        font-size: 20px;\n        padding: 10px 20px;\n    \n    }\n}\n\n@media screen and (max-width: 629px) and (min-width: 460px) {\n    .content >div > div {\n        font-size: 18px;\n    }\n\n   /* .content .title {\n        padding-top: 60px;\n    }\n*\n    .content .action {\n        padding-top: 45px;\n    }*/\n\n    .content .action a {\n        font-size: 20px;\n        padding: 10px 20px;\n    \n    }\n}\n\n@media screen and (max-width: 459px) and (min-width: 370px){\n    .content >div > div {\n        font-size: 15px;\n    }\n\n   /* .content .title {\n        padding-top: 30px;\n    }\n\n    .content .action {\n        padding-top: 45px;\n    }*/\n\n    .content .action a {\n        font-size: 20px;\n        padding: 10px 20px;\n    \n    }\n}\n\n@media screen and (max-width: 370px){\n    .content >div > div {\n        font-size: 13px;\n    }\n\n   /* .content .title {\n        padding-top: 20px;\n    }\n\n    .content .action {\n        padding-top: 30px;\n    }*/\n\n    .content .action a {\n        font-size: 13px;\n        padding: 10px 20px;\n    \n    }\n}\n\n/*@media screen and (max-width: 769px) and (min-width: 600px) {\n    .bar {\n        width: 50%;\n    }\n}*/"

/***/ }),

/***/ "./src/app/animated-bar/animated-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"bar\"  (mouseover)='hoverImg()' (mouseout)='hoverImg()'>\n    <img [src]=\"imagePath\">\n    <div class=\"content\" [@visibilityChanged]='opacity'>\n      <div [style.background-color] = \"hovColor\">\n          <div  class=\"title\">{{title}}</div>\n          <div class=\"descrition\">{{descritpion}}</div>\n          <div class=\"teh-title\">Technologies used:</div>\n          <div class=\"teh\">{{technologies}}</div>\n          <div class=\"action\">\n            <a routerLink=\"item/{{id}}\">More</a>\n          </div>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/animated-bar/animated-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnimatedBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_animations__ = __webpack_require__("./node_modules/@angular/animations/esm5/animations.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AnimatedBarComponent = /** @class */ (function () {
    function AnimatedBarComponent() {
        this.opacity = 'hide';
        this.opacityStatus = false;
    }
    AnimatedBarComponent.prototype.hoverImg = function () {
        this.opacityStatus = !this.opacityStatus;
        if (this.opacityStatus) {
            this.opacity = 'show';
        }
        else {
            this.opacity = 'hide';
        }
    };
    AnimatedBarComponent.prototype.ngOnInit = function () {
        this.hovColor = this.hovColor + 'B3';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], AnimatedBarComponent.prototype, "id", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], AnimatedBarComponent.prototype, "descritpion", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], AnimatedBarComponent.prototype, "technologies", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], AnimatedBarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], AnimatedBarComponent.prototype, "hovColor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], AnimatedBarComponent.prototype, "imagePath", void 0);
    AnimatedBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-animated-bar',
            template: __webpack_require__("./src/app/animated-bar/animated-bar.component.html"),
            styles: [__webpack_require__("./src/app/animated-bar/animated-bar.component.css")],
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["j" /* trigger */])('visibilityChanged', [
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["g" /* state */])('hide', Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["h" /* style */])({
                        opacity: 0
                    })),
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["g" /* state */])('show', Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["h" /* style */])({
                        opacity: 1
                    })),
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["i" /* transition */])('hide => show', Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["e" /* animate */])('500ms')),
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["i" /* transition */])('show => hide', Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["e" /* animate */])('500ms'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [])
    ], AnimatedBarComponent);
    return AnimatedBarComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ".content {\n    max-width: 1100px;\n    margin: 0px auto;\n    padding: 0px 35px;\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n    <router-outlet>\n    </router-outlet>\n<app-contact></app-contact>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_sticky__ = __webpack_require__("./node_modules/ng-sticky/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_tooltip__ = __webpack_require__("./node_modules/ngx-bootstrap/tooltip/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__header_header_component__ = __webpack_require__("./src/app/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__main_main_component__ = __webpack_require__("./src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__contact_contact_component__ = __webpack_require__("./src/app/contact/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__about_about_component__ = __webpack_require__("./src/app/about/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__team_item_team_item_component__ = __webpack_require__("./src/app/team-item/team-item.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__animated_bar_animated_bar_component__ = __webpack_require__("./src/app/animated-bar/animated-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__item_item_component__ = __webpack_require__("./src/app/item/item.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







//  import { SmoothScrollToDirective, SmoothScrollDirective } from "ng2SmoothScroll";








var routes = [{
        component: __WEBPACK_IMPORTED_MODULE_9__main_main_component__["a" /* MainComponent */],
        path: ''
    },
    {
        component: __WEBPACK_IMPORTED_MODULE_11__about_about_component__["a" /* AboutComponent */],
        path: 'about'
    },
    {
        component: __WEBPACK_IMPORTED_MODULE_10__contact_contact_component__["a" /* ContactComponent */],
        path: 'contact'
    },
    {
        component: __WEBPACK_IMPORTED_MODULE_14__item_item_component__["a" /* ItemComponent */],
        path: 'item/:id'
    },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_8__header_header_component__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_9__main_main_component__["a" /* MainComponent */],
                __WEBPACK_IMPORTED_MODULE_10__contact_contact_component__["a" /* ContactComponent */],
                __WEBPACK_IMPORTED_MODULE_11__about_about_component__["a" /* AboutComponent */],
                __WEBPACK_IMPORTED_MODULE_12__team_item_team_item_component__["a" /* TeamItemComponent */],
                __WEBPACK_IMPORTED_MODULE_13__animated_bar_animated_bar_component__["a" /* AnimatedBarComponent */],
                __WEBPACK_IMPORTED_MODULE_14__item_item_component__["a" /* ItemComponent */] /*,
                SmoothScrollToDirective,
                SmoothScrollDirective,*/
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* RouterModule */].forRoot(routes),
                __WEBPACK_IMPORTED_MODULE_5_ng_sticky__["a" /* NgStickyModule */],
                __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_tooltip__["a" /* TooltipModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/contact/contact.component.css":
/***/ (function(module, exports) {

module.exports = ".content {\n    max-width: 1100px;\n    margin: 0px auto;\n    color: #bbb;\n    padding: 0px 20px;\n    font-size: 14px;\n    line-height: 20px;\n}\n\n.contact-info{\n    line-height: 40px;\n}\n\n.content>div {\n    padding: 0px 10px 30px 15px;\n}\n\n.footer {\n    background-color: #393f43;\n\n    padding-top: 40px;\n    padding-bottom: 40px;\n}\n\n.form-title {\n    color: #fff;\n    font-size: 20px;\n    line-height: 28px;\n    margin-bottom: 20px;\n}\n\n.email {\n    width: 100%;\n    margin-bottom: 20px;\n    color: #252525;\n    border: none;\n    border-bottom: 1px solid #2a2f37;\n    background-color: #2f3539;\n    font-size: 14px;\n    font-weight: 400;\n    font-style: normal;\n    height: 50px;\n    border-radius: 5px 5px;\n}\n\n.message{\n    padding-left: 2%;\n    padding-right: 2%;\n    padding-top: 10px;\n    padding-bottom: 10px;\n    width: 100%;\n    margin-bottom: 20px;\n    color: #252525;\n    border: none;\n    border-bottom: 1px solid #2a2f37;\n    background-color: #2f3539;\n    font-size: 14px;\n    font-weight: 400;\n    font-style: normal;\n    height: 150px;\n    border-radius: 5px 5px;\n}\n\ninput:focus,\ntextarea:focus {\n    color: #ddd;\n    background-color: #01994d;\n    outline: none;\n}\n\nbutton {\n    font-size: 13px;\n    font-weight: 500;\n    padding: 12px 35px;\n    text-transform: uppercase;\n    border-radius: 3px 3px;\n    color: #fff;\n    background-color: #27ae60;\n    border: 1px solid #27ae60;\n    -webkit-transition: all .50s ease-in-out;\n}\n\n.contact-info i {\n    font-size: 18px;\n    padding-right: 25px;\n}\n\n.footer-menu {\n    background-color: #2f3539;\n    width: 100%;\n    margin: 0px;\n}\n\n.footer-menu-content {\n    max-width: 1100px;\n    margin: 0px auto;\n    padding: 10px 50px;;\n    font-size: 14px;\n    line-height: 26px;\n}\n\n.footer-menu-content a {\n    color: #656565;\n    text-decoration: none;\n    cursor: pointer;\n}\n\n.footer-menu-content a:hover {\n    color: #01994d;\n}"

/***/ }),

/***/ "./src/app/contact/contact.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"footer\" #content>\n  <div class=\"content row\" id=\"contact\">\n    <div class=\"col-lg-6 col-xs-12\">\n      <div class=\"form-title\">\n          Feedback\n      </div>\n      <form method=\"POST\" action=\"https://formspree.io/vladimirruzhyn@gmail.com\">\n        <input type=\"email\" name=\"email\" class=\"col-sm-12 email\" placeholder=\"Your email\">\n        <br/>\n        <textarea name=\"message\" class=\"col-sm-12 message\" placeholder=\"Your message\"></textarea>\n        <br/>\n        <button type=\"submit\">Send</button>\n      </form>\n    </div>\n    <div class=\"col-lg-6 col-xs-12\">\n        <div class=\"form-title\">\n          How to contact us\n        </div>\n        <div class=\"contact-info\">\n            <i class=\"fas fa-map-marked-alt\"></i>Bucha <br>\n            <i class=\"fas fa-phone\"></i> +38 (063) - 479 - 90 - 88<br>        \n            <i class=\"far fa-paper-plane\"></i>info@appsmob.ru<br/>\n            <i class=\"fab fa-skype\"></i>andrii.soroka           \n        </div>\n    </div>\n  </div>\n</div>\n<div class=\"footer-menu row\">\n  <div class=\"footer-menu-content\">\n    <a href='/'>\n      Main\n    </a>\n    <br/>\n    <a href='/'>\n      Portfolio\n    </a>\n    <br/>\n    <a href='/about'>\n      About Us\n    </a>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/contact/contact.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ContactComponent = /** @class */ (function () {
    function ContactComponent() {
    }
    ContactComponent.prototype.ngOnInit = function () {
    };
    ContactComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-contact',
            template: __webpack_require__("./src/app/contact/contact.component.html"),
            styles: [__webpack_require__("./src/app/contact/contact.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ContactComponent);
    return ContactComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/***/ (function(module, exports) {

module.exports = "header {\n    width: 100%;\n    background-color: white;\n    height: 90px;    \n    padding-top: 20px;\n}\n\n.navbar-default {\n    background-image: none;\n    -webkit-box-shadow: none;\n            box-shadow: none;    \n    padding: 0px 20px;\n    background-color: rgb(255, 255, 255);\n    background-color: rgba(255, 255, 255, .95); \n    border: 0px;\n    margin: 0px;\n    border-radius: 0px;\n}\n\n.is-sticky {\n    margin: 0px;\n    -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, .2);\n            box-shadow: 0 0 4px rgba(0, 0, 0, .2);\n    width: 100%;\n    /*background-color: white;*/\n    z-index: 9;\n}\n\na {\n    cursor: pointer;\n}\n\na:hover {\n    color: #01994d !important;\n}\n\n.content-sticky .container-fluid { \n    max-width: 1100px;\n}\n\n.content-sticky .container-fluid .navbar-collapse .navbar-nav {\n    float: right;\n}"

/***/ }),

/***/ "./src/app/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<header>\n  <div class=\"content-sticky\" ng-sticky  [addClass]=\"'is-sticky'\" [offSet]=\"0\" >\n    <nav class=\"navbar navbar-default\" role=\"navigation\">\n        <div class=\"container-fluid\">\n          <div class=\"navbar-header\">\n            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\n              <span class=\"sr-only\">Toggle navigation</span>\n              <span class=\"icon-bar\"></span>\n              <span class=\"icon-bar\"></span>\n              <span class=\"icon-bar\"></span>\n            </button>\n            <a class=\"navbar-brand\" routerLink=\"/\">TDT - Tartila dev team</a>\n          </div>\n          <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n            <ul class=\"nav navbar-nav\">\n              <li class=\"\">\n                  <a routerLink=\"/\">\n                    Portfolio\n                  </a>\n              </li>\n              <li>\n                  <a routerLink=\"/about\">\n                    About As\n                  </a>\n              </li>\n              <li>\n                  <a (click)=\"goToContact()\">\n                    Contact\n                  </a>\n              </li>\n            </ul>\n          </div>\n        </div>\n    </nav>\n  </div>\n</header>"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_utils__ = __webpack_require__("./node_modules/ngx-bootstrap/utils/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
        Object(__WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_utils__["a" /* setTheme */])('bs3');
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.goToContact = function () {
        var element = document.getElementById('contact');
        if (element != null) {
            element.scrollIntoView({ block: 'start', behavior: 'smooth' });
        }
    };
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-header',
            template: __webpack_require__("./src/app/header/header.component.html"),
            styles: [__webpack_require__("./src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/item/item.component.css":
/***/ (function(module, exports) {

module.exports = ".half-image {\n    width: 100%;\n    -webkit-box-shadow: 0 0 10px rgba(0,0,0,0.5);\n            box-shadow: 0 0 10px rgba(0,0,0,0.5);\n}\n\n.main-title {\n    text-align: center;\n    font-weight: bold;\n    font-size: 25px;\n    margin-top: 50px;\n    margin-bottom: 50px;\n}\n\n.technologies>b {\n    display: inline-block;\n    margin-bottom: 15px;\n    font-size: 20px;\n}\n\n.description {\n    line-height: 25px;\n    margin-bottom: 40px;\n    /*height: 155px;*/\n}\n\n.description p {\n    text-indent: 1.5em;\n    margin: 0px;\n}\n\n.image-conteiner {    \n    padding: 0 5px;\n}\n\n.list-content {\n    line-height: 25px;\n}\n\n.list-content .title {\n    width: 170px;\n    display: inline-block;\n}\n\n.content-conteiner {\n    /*padding-left: 25px;\n    padding-right: 25px;*/\n    font-size: 17px;\n}\n\np {\n    text-indent: 1.5em;\n}\n\n.worked-head {\n    font-weight: bold;\n    margin-top: 60px;\n    margin-bottom: 15px;\n    font-size: 20px;\n}\n\n.worck-list li {\n    margin-bottom: 10px;\n}\n\n.worck-list {\n    margin-bottom: 20px;\n}\n\n.container {\n    width: 100%;\n    padding: 0 25px;\n}\n\n@media screen and (max-width: 1490px) and (min-width: 1475px)  {\n\n\n    .content-conteiner {\n        font-size: 14px;\n    }\n\n}\n\n@media screen and (max-width: 1476px) and (min-width: 1201px)  {\n\n    .main-title[_ngcontent-c3] {\n        text-align: center;\n        font-weight: bold;\n        font-size: 25px;\n        margin-top: 35px;\n        margin-bottom: 35px;\n    }\n\n    .content-conteiner[_ngcontent-c3] {\n        font-size: 14px;\n    }\n\n    .description[_ngcontent-c3] {\n        line-height: 18px;\n        margin-bottom: 25px;\n    }\n\n}\n\n@media screen and (max-width: 1200px) and (min-width: 1080px)  {\n\n\n    .main-title {\n        text-align: center;\n        font-weight: bold;\n        font-size: 21px;\n        margin-top: 20px;\n        margin-bottom: 20px;\n    }\n\n    .description {\n        line-height: 18px;\n        margin-bottom: 10px;\n    }\n\n    .technologies > b {\n        display: inline-block;\n        margin-bottom: 10px;\n        font-size: 17px;\n    }\n\n    .list-content {\n        line-height: 19px;\n    }\n\n\n    .content-conteiner {\n        font-size: 15px;\n      \n    }\n}\n\n@media screen and (max-width: 1079px) and (min-width: 992px)  {\n\n\n    .main-title {\n        text-align: center;\n        font-weight: bold;\n        font-size: 20px;\n        margin-top: 20px;\n        margin-bottom: 20px;\n    }\n\n    .description {\n        line-height: 14px;\n        margin-bottom: 10px;\n    }\n\n    .technologies > b {\n        display: inline-block;\n        margin-bottom: 10px;\n        font-size: 15px;\n    }\n\n    .list-content {\n        line-height: 18px;\n    }\n\n\n    .content-conteiner {\n        font-size: 12px;\n    }\n\n}\n\n@media screen and (max-width: 992px)  {\n\n\n   \n    .content-conteiner {\n        /*padding-left: 6%;\n        padding-right: 6%;*/\n    }\n}"

/***/ }),

/***/ "./src/app/item/item.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n<div class=\"row\">\n  <div class=\"col-md-6 col-sm-12 image-conteiner\">\n    <img class=\"half-image\" [src]=\"item.imageFirstPath\">\n  </div>\n  <div class=\"col-md-6 col-sm-12 content-conteiner\">\n    <div class=\"main-title\">\n      {{item.title}}\n    </div>\n    <div class=\"description\"  [innerHTML]=\"item.fullDescription\">\n    </div>\n    <div class=\"technologies\">\n      <b>Technologies used:</b>\n\n          <div class=\"list-content\">\n          <ng-template ngFor let-value [ngForOf]=\"item.technologiesList\" let-i=\"index\">\n              <span class=\"title\">{{value.title}}:</span>{{value.content}}<br/>    \n          </ng-template>\n        </div>  \n    </div>\n  </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-md-6 col-sm-12 content-conteiner\" >\n      <div class=\"worked-head\">\n          Our developers worked on:\n      </div>\n      <ul class=\"worck-list\" *ngIf=\"item.worck.type == 'list'\">\n        <li *ngFor=\"let item of item.worck.list\">\n          {{item}}\n        </li>\n      </ul>\n      <div class=\"worck-list\" *ngIf=\"item.worck.type == 'description'\">\n          {{item.worck.list}}\n      </div>\n    </div>\n    <div class=\"col-md-6 col-sm-12 image-conteiner\">\n      <img class=\"half-image\" [src]=\"item.imageSecondPath\">\n    </div>\n</div>\n</div>\n"

/***/ }),

/***/ "./src/app/item/item.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__progect_mock__ = __webpack_require__("./src/app/progect-mock.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ItemComponent = /** @class */ (function () {
    function ItemComponent(_r) {
        this._r = _r;
    }
    ItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._r.params.subscribe(function (params) {
            var id = params['id'];
            if (__WEBPACK_IMPORTED_MODULE_2__progect_mock__["a" /* ObjectItemList */][id - 1]) {
                _this.item = __WEBPACK_IMPORTED_MODULE_2__progect_mock__["a" /* ObjectItemList */][id - 1];
            }
            console.log(_this.item);
        });
    };
    ItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-item',
            template: __webpack_require__("./src/app/item/item.component.html"),
            styles: [__webpack_require__("./src/app/item/item.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], ItemComponent);
    return ItemComponent;
}());



/***/ }),

/***/ "./src/app/main/main.component.css":
/***/ (function(module, exports) {

module.exports = "p {\n    width: 100%;\n    height: 100%;\n    background-color: black;\n}"

/***/ }),

/***/ "./src/app/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<app-animated-bar *ngFor=\"let item of projectList\" [descritpion]=\"item.descritpion\" [technologies]=\"item.technologies\" [title]=\"item.title\" [imagePath]=\"item.imagePath\" [id]=\"item.id\" >"

/***/ }),

/***/ "./src/app/main/main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__progect_mock__ = __webpack_require__("./src/app/progect-mock.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MainComponent = /** @class */ (function () {
    function MainComponent() {
    }
    MainComponent.prototype.ngOnInit = function () {
        this.projectList = __WEBPACK_IMPORTED_MODULE_1__progect_mock__["a" /* ObjectItemList */];
    };
    MainComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-main',
            template: __webpack_require__("./src/app/main/main.component.html"),
            styles: [__webpack_require__("./src/app/main/main.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MainComponent);
    return MainComponent;
}());



/***/ }),

/***/ "./src/app/progect-mock.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ObjectItemList; });
var ObjectItemList = [
    {
        id: 1,
        descritpion: 'Management system for preschool institutions',
        technologies: 'PHP, Joomla, Javascript, JQuery, Selenium, Java, JSON, HTML, CSS, Bootstrap, MySQL',
        title: 'Management system for preschool institutions',
        imagePath: './assets/babysdays.jpg',
        imageFirstPath: './assets/mobile-babys1.jpg',
        imageSecondPath: './assets/mobile-babys.jpg',
        fullDescription: '<p>A large complex management system for pre-schools.</p>' +
            '<p>The System consists of 16 modules and more than 250 unique pages. Today, the product is being used by more than 4500 of pre-school institutions.</p>' +
            '<p>The system automates all processes - from marks on the mood of a child before paying taxes.</p>' +
            '<p>The system consists of a web-based platform and a mobile application (Android, iOS)</p>',
        technologiesList: [
            {
                title: 'Web Server',
                content: 'PHP5, Joomla 3, Javascript, Jquery, Bootstrap'
            },
            {
                title: 'Mobile platform',
                content: 'JAVA, Object-C'
            },
            {
                title: 'Automatic testing',
                content: 'JAVA, Selenium, PHPUnit, Jenkin'
            }
        ],
        worck: {
            type: 'list',
            list: [
                'Development of API for mobile platforms',
                'Development and implementation of automated testing',
                'Web Platform Development',
                'Development of the structure of the database'
            ]
        }
    },
    {
        id: 2,
        descritpion: 'Organic Food Store',
        technologies: 'PHP, Magento 2, Javascript, JQuery, RequireJS, KnockoutJS, Selenium, Python, Google API, MySQL',
        title: 'Little lunch',
        imagePath: './assets/littlelunch.jpg',
        imageFirstPath: './assets/littlelunch1.jpg',
        imageSecondPath: './assets/littlelunch2.jpg',
        fullDescription: '<p>Organic Food Store.</p>' +
            '<p>The shop belongs to "Little Lunch" ltd and sells the products of the company which consists of organic soups, smoothies, and brooks.</p>' +
            '<p>The firm is one of the foremost in the European market.</p>' +
            '<p>The store uses advanced technologies to automate the sales process and track the buyer\'s actions</p>',
        technologiesList: [
            {
                title: 'Web Server',
                content: 'PHP7, Magento 2, Javascript, JQuery, RequireJS, KnockoutJS, MySQL'
            },
            {
                title: 'Automatic testing',
                content: 'JAVA, Selenium, PHPUnit, Python, JMeter'
            },
            {
                title: 'Additional Technologies',
                content: 'Google analytics, Google maps, PayPal, Braintree'
            }
        ],
        worck: {
            type: 'list',
            list: [
                'Development of modules for Magento 2 (Google analytics, Address validation, Payment, etc.)',
                'Development of functional tests',
                'Development and implementation of load testing',
                'Unit Test Development'
            ]
        }
    },
    {
        id: 3,
        descritpion: 'Logistics Management System',
        technologies: 'Python, WS, Backbone, PgSQL, Redis, Mongo',
        title: 'Logistic',
        imagePath: './assets/logistics.jpg',
        hovColor: '#f44242'
    },
    {
        id: 4,
        descritpion: 'Job search in all soy networks and chats',
        technologies: 'Go, Mongo, Redis, MySQL, Angular 5',
        title: 'Remote Work',
        imagePath: './assets/remote work.jpg',
        hovColor: '#f44242'
    },
    {
        id: 5,
        descritpion: 'Playground for sale photos',
        technologies: 'ReactJs, NodeJs, MySQL, Modx',
        title: 'Picstation',
        imagePath: './assets/picstation.jpg',
        imageFirstPath: './assets/picstation1.jpg',
        imageSecondPath: './assets/picstation2.jpg',
        fullDescription: '<p>Playground for sale photos</p>' +
            '<p>The platform allows you to sell your products as photographers to professionals and amateurs.</p>' +
            '<p>The system guarantees that the make will be concluded without fraud - the buyer will receive a photo, and the photographer money.' +
            '<p>The search system allows the buyer to choose the most appropriate ones from a large number of photos. This greatly increases the search efficiency.</p>',
        technologiesList: [
            {
                title: 'Web Server',
                content: 'NodeJs, MySQL, Javascript, ReactJs'
            }
        ],
        worck: {
            type: 'description',
            list: [
                'The project was fully implemented Tartila dev team'
            ]
        },
        hovColor: '#f44242'
    },
    {
        id: 6,
        descritpion: 'The platform for finding work / employees and conducting IT projects',
        technologies: 'Python, Flask, MySQL, Angular 5',
        title: 'Bit Jobs',
        imagePath: './assets/bit jobs.jpg',
        imageFirstPath: './assets/bit jobs1.jpg',
        imageSecondPath: './assets/bit jobs2.jpg',
        fullDescription: '<p>The platform for finding work / employees and conducting IT projects</p>' +
            '<p>Bit Jobs includes all the necessary functionality for IT projects, which has a simple and intuitive interface. This allows you to run the project directly in the system.</p>' +
            '<p>The system contains detailed information about the employee and employer, as well as their work history in the system that allows you to choose the best employees and employers.</p>' +
            '<p>The system supports payment of work that allows to reduce fraud on the part of the customer.</p>',
        technologiesList: [
            {
                title: 'Web Server',
                content: 'NodeJs, MySQL, Javascript, ReactJs'
            }
        ],
        worck: {
            type: 'description',
            list: [
                'The alpha version of the system with all the functionality was developed by Tartila dev team'
            ]
        },
        hovColor: '#f44242'
    }
];


/***/ }),

/***/ "./src/app/team-item/team-item.component.css":
/***/ (function(module, exports) {

module.exports = ".item-team img{\n    height: 128px;\n    width: 128px;\n    border: 1px solid #e4e4e4;\n    border-radius: 64px;    \n    font-size: 18px;\n    font-weight: 400;\n    line-height: 30px;\n    letter-spacing: 0.4px;\n    margin-bottom: 10px;\n}\n\n.item-team {\n    width: 250px;\n    text-align: center;\n    display: inline-block;\n    margin: 0px 2px 60px 2px;\n    padding: 0px 20px;\n}\n\n.position {\n    font-size: 100%;\n    font-weight: bold;\n}\n\n.full-name {\n    font-style: italic;\n}"

/***/ }),

/***/ "./src/app/team-item/team-item.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"item-team\">\n  <img src=\"/assets/dima.jpg\">\n  <p class=\"position\">\n    Web-разработчик\n  </p>\n  <p class=\"full-name\">\n    Dmitry Dmitry\n  </p>\n  <p class=\"description\">\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n  </p>\n</div>\n"

/***/ }),

/***/ "./src/app/team-item/team-item.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeamItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TeamItemComponent = /** @class */ (function () {
    function TeamItemComponent() {
    }
    TeamItemComponent.prototype.ngOnInit = function () {
    };
    TeamItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-team-item',
            template: __webpack_require__("./src/app/team-item/team-item.component.html"),
            styles: [__webpack_require__("./src/app/team-item/team-item.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TeamItemComponent);
    return TeamItemComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map